from minio import Minio
from minio.error import S3Error
import sys
import os
import glob
import time
import tarfile
from shutil import make_archive


files =  "../test"
zip = "zip"
path = os.path.basename((files))

way = "backup"+"/"+path+time.strftime('_%Y-%m-%d-%H:%M:%S')
z = path+time.strftime('_%Y-%m-%d-%H:%M:%S')
if os.path.exists(files) == True:
    if os.access(files, os.R_OK) == True:
        if os.path.exists(files) == True:
            if os.access(files, os.W_OK) == True:
                make_archive(way, zip, files)
                z = make_archive(way, zip, files)


def main():
    client = Minio(
        endpoint="localhost:9000",
        access_key="testtest",
        secret_key="testtest",
        secure=False
    )

    found = client.bucket_exists("test")
    if not found:
        client.make_bucket("test")
    else:
        print("there already")

    client.fput_object(
        "test", z, z,
    )
    print(
        "Everything allright!!!"
    )


if __name__ == "__main__":
    try:
        main()
    except S3Error as exc:
        print("error occurred.", exc)
